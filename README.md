# Json-Reader

Version 0.1 (Beta) you can view your json files in simply tree-based viewer

## Installation

You need the next packeges for succesful build:
- qtdeclarative5-dev
- qt5-qmake
- g++
- git

then:
```
git clone https://codeberg.org/morlacson/Json-Reader.git
cd Json-Reader
qmake JSON-Reader.pro
make
```
Clean:
```
make clean
```
Run:
```
./JSON-Reader
```