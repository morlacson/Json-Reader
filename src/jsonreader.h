#ifndef JSONREADER_H
#define JSONREADER_H

#include <QMainWindow>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QTreeWidgetItem>

QT_BEGIN_NAMESPACE
namespace Ui { class JsonReader; }
QT_END_NAMESPACE

class JsonReader : public QMainWindow
{
    Q_OBJECT

public:
    JsonReader(QWidget *parent = nullptr);
    ~JsonReader();

private slots:
    void on_actionOpen_triggered();

private:
    Ui::JsonReader *ui;
    QJsonDocument current;
    QJsonArray array;
    QJsonObject object;
    void loadJson(const QString &fileName);
    void saveJson(const QString &fileName);
    QTreeWidgetItem *add(QJsonValue a, QVariant v);
};
#endif // JSONREADER_H
