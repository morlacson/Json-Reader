#include "jsonreader.h"
#include "ui_jsonreader.h"

JsonReader::JsonReader(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::JsonReader)
{
    ui->setupUi(this);
    ui->treeWidget->setColumnCount(2);
    this->show();
}

JsonReader::~JsonReader()
{
    delete ui;
}


void JsonReader::on_actionOpen_triggered()
{
    QString tmp = QFileDialog::getOpenFileName(this, "Select JSON", QString::fromLocal8Bit(qgetenv("HOME")), tr("Json file (*.json)"));
    this->loadJson(tmp);
}

void JsonReader::loadJson(const QString &fileName)
{
    QFile jsonFile(fileName);
    jsonFile.open(QFile::ReadOnly);
    current = QJsonDocument::fromJson(jsonFile.readAll());
    ui->treeWidget->clear();
    if(current.isArray())
    {
        array = current.array();
        for(int i = 0; i < array.count(); ++i)
        {
            ui->treeWidget->addTopLevelItem(this->add(array.at(i), i));
        }
    } else if(current.isObject())
    {
        object = current.object();
        for(int i = 0; i < object.keys().size(); ++i)
        {
            ui->treeWidget->addTopLevelItem(this->add(object.value(object.keys()[i]), object.keys()[i]));
        }
    }
}

void JsonReader::saveJson(const QString &fileName)
{
    QFile jsonFile(fileName);
    jsonFile.open(QFile::WriteOnly);
    jsonFile.write(current.toJson());
}

QTreeWidgetItem* JsonReader::add(QJsonValue a, QVariant v)
{
    if(a.isObject())
    {
        QJsonObject c = a.toObject();

        QTreeWidgetItem *b = new QTreeWidgetItem();
        for(int i = 0; i < c.keys().count(); ++i)
        {
            b->addChild(this->add(c[c.keys()[i]], c.keys()[i]));
        }
        b->setData(0, Qt::EditRole, v.toString());
        return b;
    } else if(a.isString())
    {
        QTreeWidgetItem *b = new QTreeWidgetItem();
        b->setData(0, Qt::EditRole, v.toString());
        b->setData(1, Qt::EditRole, a.toString());
        return b;
    } else if(a.isBool())
    {
        QTreeWidgetItem *b = new QTreeWidgetItem();
        if(a.toBool())
        {
            b->setData(0, Qt::EditRole, v.toString());
            b->setData(1, Qt::EditRole, "true");
        } else
        {
            b->setData(0, Qt::EditRole, v.toString());
            b->setData(1, Qt::EditRole, "false");
        }
        return b;
    } else if(a.isArray())
    {
        QTreeWidgetItem *b = new QTreeWidgetItem();
        for (int i = 0; i < a.toArray().count() ; ++i )
        {
            b->addChild(this->add(a.toArray().at(i), i));
        }
        b->setData(0, Qt::EditRole, v);
        return b;
    } else if(a.isDouble())
    {
        QTreeWidgetItem *b = new QTreeWidgetItem();
        b->setData(0, Qt::EditRole, v.toString());
        b->setData(1, Qt::EditRole, QString::fromLocal8Bit(QByteArray::number(a.toDouble())));
        return b;
    }else
    {
        return nullptr;
    }
}






